# Copyright 2019 Jorrit Fahlke <jorrit@jorrit.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

TESTS = luks

.PHONY: test $(addprefix test/,$(TESTS))
test: $(addprefix test/,$(TESTS))
$(addprefix test/,$(TESTS)): test/%: mount-cryptsetup.tar
	./run-in-vm -s --temp-sdb -a mount-cryptsetup.tar -p coreutils,cryptsetup,e2fsprogs,kmod test/$*-script
