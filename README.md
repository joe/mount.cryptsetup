Mount helper for cryptsetup-supported volumes
=============================================

This provides `mount.cryptsetup` decrypting and mounting encrypted volumes in
one go.  This works for all encrypted volumes supported by cryptsetup and all
single-volume file systems.  It can be used either directly, or by invoking
mount with the `-t <encryption_type>[.<fstype>]` option.

During mount, cryptsetup will ask for the volume passphrase if appropriate.
The decryption is set up in such a way that the kernel will automatically tear
down the dm-crypt device when it is no longer in use, i.e. when the filesystem
is unmounted.

Unmounting can be done using the normal `umount` program.  However, it is
neccessary to specify the mount point, the encrypted volume specified during
mount cannot be used here, because `umount` does not understand the relation
between the encrypted and the decrypted volume.

Alias names
-----------

There are various symlinks pointing to `mount.cryptsetup`.  These are used to
make `mount -t <encryption_type>` work, and to automatically handle encrypted
volume whose type mount detects using `blkid` when not type is explicitly
specified. These names are:
- `mount.crypto_LUKS` luks encryption detected by blkid
- `mount.luks` convenience name to make `mount -t luks` work

Options
-------

Options can be specified either with `mount -o cryptsetup.<opt>[=<value>]` or
in a similar fashion in `/etc/fstab`.  options starting with `cryptsetup.` are
passed to `cryptsetup`, any other options are passed to the `mount` invocation
that mount the filesystem.  Options `ro` and `discard` (without qualification)
are also recognized, and enable `cryptsetup`s `--readonly` and
`--allow-discards` options.
