# Copyright 2019 Jorrit Fahlke <jorrit@jorrit.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

.PHONY: all
all:

ALT_NAMES =					\
	luks crypto_LUKS			\
	tcrypt truecrypt			\
	vera veracrypt				\
	dmcrypt					\
	loopaes

mount-cryptsetup.tar: mount.cryptsetup
	rm -rf "$@.d" "$@.fakedb" "$@.list"
	fakeroot -s "$@.fakedb"			\
	  $(MAKE) DESTDIR="$@.d" install
	( cd "$@.d" && find ! -type d -print0 ) >"$@.list"
	fakeroot -i "$@.fakedb"					\
	  tar -cf- -C "$@.d" --null -T- >"$@.tmp" <"$@.list"
	rm -rf "$@.d" "$@.fakedb" "$@.list"
	mv "$@.tmp" "$@"

define install_links =
$(foreach name,$(ALT_NAMES),
  ln -sf mount.cryptsetup "$(DESTDIR)"/sbin/mount.$(name)
)
endef
.PHONY: install
install: all
	install -m 755 -D -t "$(DESTDIR)"/sbin mount.cryptsetup
	$(install_links)

define uninstall_links =
$(foreach name,$(ALT_NAMES),
  rm -f "$(DESTDIR)"/sbin/mount.$(name)
)
endef
.PHONY: uninstall
uninstall:
	rm -f "$${DESTDIR}"/sbin/mount.cryptsetup
	$(uninstall_links)

include test/dir.mk
